<?php
namespace AppBundle\Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostcreateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('cmd:postcreate')
            ->setDescription('Create post - params: userid ( required ), title, body')
            ->addArgument(
                'userId',
                InputArgument::REQUIRED,
                'What is user\'s id?'
            )
                ->addArgument(
                'title',
                InputArgument::OPTIONAL,
                'What is title of post?'
            )
                ->addArgument(
                'body',
                InputArgument::OPTIONAL,
                'What is body of post?'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        try {
            $url =  'http://api.fatbee.org/posts';
            $data = array(
                'userId' => $input->getArgument('userId'),
                'title' => $input->getArgument('title'),
                'body' => $input->getArgument('body')
            );

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data),
                ),
            );
            $context  = stream_context_create($options);
            $result = @file_get_contents($url, false, $context);
            if ( $result === false ) throw new \Exception("Error adding post");
            $output->writeln( "POST added: \n\r" . $result );
        }
        catch ( Exception $e) {
            $output->writeln($e);
        }
    }
}