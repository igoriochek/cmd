<?php
namespace AppBundle\Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostreportCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('cmd:postreport')
            ->setDescription('Post reports - one of keys: withcom, withnocom, maxcom is used')
            
            ->addOption(
               'withcom',
               null,
               InputOption::VALUE_NONE,
               'Count all posts with comments'
            )
            ->addOption(
               'withnocom',
               null,
               InputOption::VALUE_NONE,
               'Count all posts without comments'
            )
            ->addOption(
               'maxcom',
               null,
               InputOption::VALUE_NONE,
               'Count all posts with maximum comments'
            );
        
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        try {
            $data = @file_get_contents("http://api.fatbee.org/comments");
            if ( $data === false ) throw new \Exception("POST not found");
            $arrayOfJsonComments = json_decode($data);
            

            $postsWithComments = array();

            foreach ( $arrayOfJsonComments as $key => $value ) {

                if ( !empty($postsWithComments[$arrayOfJsonComments[$key]->postId]) ) $postsWithComments[$arrayOfJsonComments[$key]->postId]++;
                else $postsWithComments[$arrayOfJsonComments[$key]->postId] = 1;
            }
            
            if ( $input->getOption("withcom")) {
                
                foreach ( $postsWithComments as $key => $value ) {
                    $result .= $result ? ", ".$key : $key;
                }
                $output->writeln( "POSTs with comments: \n\r" . $result );
            }
            else if ( $input->getOption("withnocom") ) {
                $data = @file_get_contents("http://api.fatbee.org/posts");
                if ( $data === false ) throw new \Exception("POST not found");
                $arrayOfJsonPosts = json_decode($data);
                $posts = array();
                
                foreach ( $arrayOfJsonPosts as $key => $val ) {
                    $posts[$val->id] = $val->id;
                }
                
                $postNoComment = array_diff_key($posts, $postsWithComments);

                if (is_array($postNoComment)) {
                    foreach ( $postNoComment as $key => $val ) {
                        $result .= $result ? ", ".$key : $key;
                    }
                }
                else {
                    $result = " empty ";
                }
                
                $output->writeln( "POSTs with no comments: \n\r" . $result );
            }
            else if ($input->getOption("maxcom") ) {
                $maxs = array_keys($postsWithComments, max($postsWithComments));
                if (is_array($maxs)) {
                    foreach ( $maxs as $val ) {
                        $result .= $result ? ", ".$val : $val;
                    }
                }
                else {
                    $result = $maxs;
                }
                $output->writeln( "POST id(s) with max comments: \n\r" . $result );
            }
            else {
                $output->writeln( "Please use option for report \n\r" );
            }
            
            
        }
        catch ( Exception $e) {
            $output->writeln($e);
        }
    }
}