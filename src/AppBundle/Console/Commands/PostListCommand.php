<?php

namespace AppBundle\Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;

class PostlistCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('cmd:postlist')
            ->setDescription('Show all posts');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $json = file_get_contents("http://api.fatbee.org/posts");
        $array = json_decode($json);
        $a = array();
        
        foreach ( $array as $key => $value ) {

            $a[] = array($array[$key]->id , $array[$key]->userId, substr( $array[$key]->title, 0 , 20), substr($array[$key]->body, 0, 20) );
            $a[] = new TableSeparator();
        }
        
        $table = new Table($output);
        $table
            ->setHeaders(array('ID', 'User id', 'Title', 'Body'))
            ->setRows($a)
        ;
        $table->render();
    }
}