<?php
namespace AppBundle\Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostremoveCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('cmd:postremove')
            ->setDescription('Remove post - params: post id ')
            ->addArgument(
                'postId',
                InputArgument::REQUIRED,
                'What  is post id you need to remove?'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        try {
            
            $id = $input->getArgument('postId');
            $url =  'http://api.fatbee.org/posts/' . $id;

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'DELETE',
                ),
            );
            $context  = stream_context_create($options);
            $result = @file_get_contents($url, false, $context);
            if ( $result === false ) throw new \Exception("POST not found");
            $output->writeln( "POST $id removed  \n\r" ); 
        }
        catch ( Exception $e) {
            $output->writeln($e);
        }
    }
}

