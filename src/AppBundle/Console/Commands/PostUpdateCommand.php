<?php
namespace AppBundle\Console\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostupdateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('cmd:postupdate')
            ->setDescription('Update post - params: post id, userid, title, body')
            ->addArgument(
                'postid',
                InputArgument::REQUIRED,
                'What post id is needed to update?'
            )
            ->addArgument(
                'userId',
                InputArgument::REQUIRED,
                'What is user\'s id?'
            )
                ->addArgument(
                'title',
                InputArgument::OPTIONAL,
                'What is title of post?'
            )
                ->addArgument(
                'body',
                InputArgument::OPTIONAL,
                'What is body of post?'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $id = $input->getArgument('postid');
            $url =  'http://api.fatbee.org/posts/'.$id;
            $data = array(
                'userId' => $input->getArgument('userId'),
                'title' => $input->getArgument('title'),
                'body' => $input->getArgument('body')
            );

            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'PUT',
                    'content' => http_build_query($data),
                ),
            );
            $context  = stream_context_create($options);
            $result = @file_get_contents($url, false, $context);
            if ( $result === false ) throw new \Exception("POST not found");
            $output->writeln( "POST $id updated: \n\r" . $result );
        }
        catch ( Exception $e) {
            $output->writeln($e);
        }
    }
}

