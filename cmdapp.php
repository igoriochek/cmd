#!/usr/bin/env php
<?php
// application.php

require __DIR__.'/vendor/autoload.php';

use AppBundle\Console\Commands\GreetCommand;
use AppBundle\Console\Commands\PostlistCommand;
use AppBundle\Console\Commands\PostcreateCommand;
use AppBundle\Console\Commands\PostremoveCommand;
use AppBundle\Console\Commands\PostupdateCommand;
use AppBundle\Console\Commands\PostreportCommand;
use Symfony\Component\Console\Application;

$application = new Application();
//$application->add(new GreetCommand());
//$application->add(new PostlistCommand());
$application->add(new PostcreateCommand());
$application->add(new PostremoveCommand());
$application->add(new PostupdateCommand());
$application->add(new PostreportCommand());

$application->run();